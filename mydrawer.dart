import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
            child: ListView(
            children:<Widget>[
              ListTile(
                title: Text("TVs & Application",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.home,color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
                ListTile(
                title: Text("Bakety",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.category, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
              Divider(),
               ListTile(
                title: Text("Dairy & Eggs",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.contact_mail, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Cans & Jars",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.settings_input_antenna, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Pantry",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
                ListTile(
                title: Text("Healthy Products",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Breakfast Food",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Chocolate & Snacks",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Frozen & Chiled",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
               ListTile(
                title: Text("Beverages & Juices",style: TextStyle(color:Colors.black,fontSize: 15,)
                ),
                //leading: Icon(Icons.logout, color: Colors.blue,size: 25,),
                trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                onTap: (){},
              ),
            ],
          ),
   );
  }
}