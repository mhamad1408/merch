import 'package:flutter/material.dart';
import 'package:march/screen/home.dart';
import 'package:march/screen/profile.dart';
import 'package:march/screen/search.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mobtech',
      home:MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
 
 TabController _tabController;

void initState(){
  super.initState();
  _tabController = TabController(length: 3, vsync: this);
}

void dispose(){
  super.dispose();
  _tabController.dispose();
}
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      body: TabBarView(
      children: <Widget>[
        Home(),
        Search(),
        ProfilePage(),
        ],
          controller: _tabController,
      ),
       bottomNavigationBar: Container(
       padding: EdgeInsets.all(16.0),
            child: ClipRRect(
         borderRadius: BorderRadius.all(Radius.circular(50.0),
         ),
              child: Container(
           color: Colors.black54,
                child: TabBar(
             labelColor: Colors.black,
             unselectedLabelColor: Colors.white,
             labelStyle: TextStyle(fontSize:10.0),
             indicator: UnderlineTabIndicator(borderSide: BorderSide(color: Colors.black54,width: 0.0),
             insets:EdgeInsets.fromLTRB(50, 0, 50, 0),
             
             ),
             indicatorColor:Colors.black54,
             tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home, size: 24,),
              child: Text('Home'),
            ),
            Tab(
               icon: Icon(Icons.search,size: 24,),
              child: Text('Search'),
            ),
            Tab(
               icon: Icon(Icons.person_outline_outlined,size: 24,),
              child: Text('Profile'),
            ),
           ],
           controller: _tabController,
           ),
         ),
       ),
     ),
    );
  }
}