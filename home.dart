// ignore: import_of_legacy_library_into_null_safe
//import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:march/screen/mydrawer.dart';
//import 'package:flutter/rendering.dart';
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  MyArticles (String imageVal,String heading,String subHeading,){
    return Container (
     width:150,
     child:new Row(
     mainAxisAlignment: MainAxisAlignment.spaceBetween,
     children:<Widget> [
      Expanded(
      child: Wrap(
       children:<Widget>[
         Image.asset(imageVal,height: 100,width: 200,fit: BoxFit.fitWidth),
         Directionality(
         textDirection: TextDirection.ltr,
         child:ListTile(
           leading:Wrap(
          children: <Widget>[
           Container(
            child:Text(heading,textAlign:TextAlign.right,style: TextStyle(fontSize:16,fontWeight: FontWeight.w600),), 
           ),
           Container(
            child:Text(subHeading,textAlign:TextAlign.right,style: TextStyle(color:Colors.blue),),
           ),
          ],
           ),
         ) ,
         ),
       ],
       ),
      ),
     ],
     ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Marche',
      home: Scaffold(
        appBar:AppBar(
          title: Text('Marche'),
          centerTitle: true,
          elevation: 6,
          ) ,
          drawer:MyDrawer() ,
        body: ListView(
        scrollDirection: Axis.vertical,
        children:<Widget>[
         Container(
            height: 150.0,
            width: double.infinity,
            child: Carousel(
              images: [
              Image.network('https://images.unsplash.com/photo-1554151228-14d9def656e4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=333&q=80', fit: BoxFit.cover,),
              Image.network('https://images.unsplash.com/photo-1554151228-14d9def656e4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=333&q=80', fit: BoxFit.cover,),
              Image.network('https://images.unsplash.com/photo-1554151228-14d9def656e4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=333&q=80', fit: BoxFit.cover,),
               //Image.asset('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',fit: BoxFit.cover),
               //Image.asset('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',fit: BoxFit.cover),
               //.asset('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',fit: BoxFit.cover),
            ],

            dotSpacing: 20,
            dotColor: Colors.white,
            dotBgColor: Colors.blue.withOpacity(0.5),
            overlayShadow: true,
            overlayShadowColors: Colors.blue,
            ),
         ),
        //   // End Carsouel
              Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
      Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      //      decoration: BoxDecoration(    
      //      border: Border(  
      //       right: BorderSide(width: 4, color: Colors.lightBlue.shade900),
      // ),
      // ),
      child:Text('Brands',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
   Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
  //     decoration: BoxDecoration(    
  //  border: Border(  
  //           right: BorderSide(width: 4, color: Colors.lightBlue.shade900),
  //     ),
  //     ),
       child:Text('Show More',textAlign: TextAlign.right,//style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800),
       ),
     ),
    ],
     ),
      // Row(
         //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // children: <Widget>[
          Stack(
            children:<Widget>[
              Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," ValHala",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Cread",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Ezio",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
      ],
      ),
       ),
     ),
       ],
    ),
     //New Arrivalls
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('New Arrivalls',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
       Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
       child:Text('Show More',textAlign: TextAlign.right,//style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800),
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
                          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," ValHala",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Cread",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Ezio",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
      ],
      ),
       ),
     ),

        ],
       ),
     // ],
     //),

     //Special Offers
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      //      decoration: BoxDecoration(    
      //      border: Border(  
      //       right: BorderSide(width: 4, color: Colors.lightBlue.shade900),
      // ),
      // ),
      child:Text('Special Offers',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),

       Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
  //     decoration: BoxDecoration(    
  //  border: Border(  
  //           right: BorderSide(width: 4, color: Colors.lightBlue.shade900),
  //     ),
  //     ),
       child:Text('Show More',textAlign: TextAlign.right,//style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800),
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
           Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," ValHala",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Cread",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            width:150,
            height:200,
            child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Ezio",""),
           //child:MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg'," عقيدة القتلة"," عقيدة القتلة",'person_outline_outlined','rating'),
          ),
      ],
      ),
       ),
     ),
        ],
       ),
        ],
        ),
      ),
    );
  }
}